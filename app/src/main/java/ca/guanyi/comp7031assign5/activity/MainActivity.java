package ca.guanyi.comp7031assign5.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.guanyi.comp7031assign5.Data.LocationListAdapter;
import ca.guanyi.comp7031assign5.Data.LongLatLocation;
import ca.guanyi.comp7031assign5.R;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout_activity_main)
    DrawerLayout drawerLayout;
    @BindView(R.id.drawer_layout_list_view)
    ListView drawerLytListView;
    @BindView(R.id.txtvInfo) TextView tvInfo;
    TextView tv;
    LocationListAdapter adapter;
    GoogleMap map;
    Location currentLocation;
    LongLatLocation selectedLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        final ArrayList<LongLatLocation> locationList = new ArrayList<>();
        locationList.add(new LongLatLocation("BCIT", -123.000907, 49.248742));
        locationList.add(new LongLatLocation("SFU", -122.924645, 49.280755));


        toolbar.inflateMenu(R.menu.toolbar_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Settings")) {
                    Log.i("TEST", "tHIS IS THE MENU");
                }
                return true;
            }
        });
        tv = new TextView(this);

        //if we just use tv.setHeight(toolbar.getHeight()), we get only 0 height, because the toolbar is not drawn yet.
        //Use the following code. If we need to copy the height of a toolbar to textView, we need to let the textView
        //to get an observer to listen when the toolbar gets the height set, then at the same time, set the toolbar's
        //height to the textView
        tv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                tv.setHeight(toolbar.getHeight());
            }
        });


        tv.setBackgroundColor(Color.DKGRAY);
        tv.setText("Title");
        drawerLytListView.addHeaderView(tv, "", false);
        adapter = new LocationListAdapter(this, R.layout.location_list, locationList);
        drawerLytListView.setAdapter(adapter);

        drawerLytListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedLocation = (LongLatLocation) parent.getItemAtPosition(position);
                LatLng latLng = selectedLocation.getLatLng();
                map.clear();
                map.addMarker(new MarkerOptions().position(latLng)).setTitle(selectedLocation.getLocationName());
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);


                //in the following part, change the tvinfo to snatch bar
                if (selectedLocation != null && currentLocation != null) {
                    int distance = (int) currentLocation.distanceTo(selectedLocation.getLocation());
                    tvInfo.append("Distance to " + selectedLocation.getLocationName() + " is " + distance + " \n");
                    tvInfo.append("--------------------------------------\n");
                    if (distance < 1000) {
                        Snackbar bar = Snackbar.make(drawerLayout, "Distance to " + selectedLocation.getLocationName() + " is " + distance + " meters.", Snackbar.LENGTH_LONG);
                        bar.show();
                    }
                }
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                googleMap.addMarker(new MarkerOptions().position(locationList.get(0).getLatLng()).title("BCIT"));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationList.get(0).getLatLng(), 15));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                currentLocation = location;
                tvInfo.append("Service location changed \n");
                tvInfo.append("Provider " + location.getProvider() + "\n");
                tvInfo.append("Accuracy " + location.getAccuracy()+ "\n");
                tvInfo.append("Altitude " + location.getAltitude()+ "\n");
                tvInfo.append("Longitude " + location.getLongitude()+ "\n");
                tvInfo.append("Latitude " + location.getLatitude()+ "\n");
                tvInfo.append("Speed " + location.getSpeed()+ "\n");
                tvInfo.append("Time " + location.getTime()+ "\n");
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
                tvInfo.append("Service status changed\n");
                tvInfo.append("String s is " + s + "\n");
                tvInfo.append("int i is " + i + "\n");
                tvInfo.append("int i is " + bundle.toString() + "\n");

            }

            @Override
            public void onProviderEnabled(String s) {
                tvInfo.append("Service Started with provider " + s + "\n");
            }

            @Override
            public void onProviderDisabled(String s) {
                tvInfo.append("Service Stopped " + s + "\n");
            }
        };
        Log.i("Permission", "PackageManager.PERMISSION_GRANTED is " + (Integer.toString(PackageManager.PERMISSION_GRANTED)));
        Log.i("Permission", "Manifest.permission.ACCESS_FINE_LOCATION is " + Manifest.permission.ACCESS_FINE_LOCATION);
        Log.i("Permission", "Manifest.permission.ACCESS_COARSE_LOCATION is " + Manifest.permission.ACCESS_FINE_LOCATION);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 50, locationListener);
        adapter.notifyDataSetChanged();
    }

}

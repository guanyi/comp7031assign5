package ca.guanyi.comp7031assign5.Data;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.guanyi.comp7031assign5.R;

/**
 * Created by Guanyi on 10/18/2016.
 */

public class LocationListAdapter extends ArrayAdapter {
    private Context context;
    private ArrayList<LongLatLocation> longLatLocationList;

    public LocationListAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
        this.context = context;
        longLatLocationList = (ArrayList<LongLatLocation>) objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.location_list, parent, false);
        //need to pass rowView to butterknife, because this rowView is the actual view holds information
        //convertView maybe not necessary at all. Tomorrow needs to check more API to see why we need convertView
        ViewHolder holder = new ViewHolder(rowView);

// the following 6 lines of code can replace bufferknife code. Butter knife here needs ot use an inner class.
// it does not add too much benefit unless the rowView is very complicated.

//        TextView txtvLocationName = (TextView) rowView.findViewById(R.id.txtvLocationName);
//        txtvLocationName.setText(longLatLocationList.get(position).getLocationName());
//        TextView txtvLongitudeValue = (TextView) rowView.findViewById(R.id.txtvLongitudeValue);
//        txtvLongitudeValue.setText(Double.toString(longLatLocationList.get(position).getLongitude()));
//        TextView txtvLatitudeValue = (TextView) rowView.findViewById(R.id.txtvLatitudeValue);
//        txtvLatitudeValue.setText(Double.toString(longLatLocationList.get(position).getLatitude()));

        holder.txtvLocationName.setText(longLatLocationList.get(position).getLocationName());
        holder.txtvLongitudeValue.setText(Double.toString(longLatLocationList.get(position).getLongitude()));
        holder.txtvLatitudeValue.setText(Double.toString(longLatLocationList.get(position).getLatitude()));
        return rowView;
    }

    static class ViewHolder {
        @BindView(R.id.txtvLocationName) TextView txtvLocationName;

        @BindView(R.id.txtvLongitudeValue) TextView txtvLongitudeValue;

        @BindView(R.id.txtvLatitudeValue) TextView txtvLatitudeValue;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

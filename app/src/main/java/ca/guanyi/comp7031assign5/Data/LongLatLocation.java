package ca.guanyi.comp7031assign5.Data;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Guanyi on 10/18/2016.
 */

public class LongLatLocation {
    private String locationName;
    private double longitude;
    private double latitude;

    public LongLatLocation(String locationName, double longitude, double latitude) {
        this.locationName = locationName;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public LatLng getLatLng() {
        return new LatLng(latitude, longitude);
    }

    public Location getLocation() {
        Location location = new Location(locationName);
        location.setLongitude(longitude);
        location.setLatitude(latitude);
        return location;
    }
}
